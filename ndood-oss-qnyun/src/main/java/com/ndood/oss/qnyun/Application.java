package com.ndood.oss.qnyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * https://gitee.com/xuliugen/oss-demo
 * https://github.com/qiniu/java-sdk
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
