package com.ndood.oss.qnyun.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 01112026 on 14-7-29.
 */
public class FileUtils {
    /**
     * 安全关闭输入流
     *
     * @param inputStream
     */
    public static void closeStream(InputStream inputStream) {
        if (inputStream == null) return;
        try {
            inputStream.close();
        } catch (IOException e) {

        }
    }

    /**
     * 安全关闭输出流
     *
     * @param outputStream
     */
    public static void closeStream(OutputStream outputStream) {
        if (outputStream == null) return;
        try {
            outputStream.close();
        } catch (IOException e) {

        }
    }

    /**
     * 把输入流写入到文件内
     * @param inputStream
     * @param file
     * @throws IOException
     */
    public static void copyInputStreamToFile(InputStream inputStream, File file) throws IOException {
        org.apache.commons.io.FileUtils.copyInputStreamToFile(inputStream, file);
    }

    /**
     * 把文件移动到目标文件
     * @param filePath 当前目录文件
     * @param dirPath  目标目录文件
     * @throws IOException
     */
    public static void moveFile(File filePath, File dirPath) throws IOException {
        org.apache.commons.io.FileUtils.moveFile(filePath, dirPath);
    }

    public static void copyFile(File filePath, File dirPath) throws IOException {
        org.apache.commons.io.FileUtils.copyFile(filePath, dirPath);
    }

    /**
     * 删除单个文件
     *
     * @param sPath 被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }

    /**
     * 删除目录（文件夹）以及目录下的文件
     *
     * @param sPath 被删除目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String sPath) {
        //如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!sPath.endsWith(File.separator)) {
            sPath = sPath + File.separator;
        }
        File dirFile = new File(sPath);
        //如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        //删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            //删除子文件
            if (files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) break;
            } //删除子目录
            else {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) break;
            }
        }
        if (!flag) return false;
        //删除当前目录
        if (dirFile.delete()) {
            return true;
        } else {
            return false;
        }
    }

    public static List<String> listFiles(String dirPath, String baseRoot) {
        if (dirPath.endsWith("/")) {
            dirPath = dirPath.substring(0, dirPath.length() - 1);
        }
        if (!dirPath.startsWith("/")) {
            dirPath = "/" + dirPath;
        }
        List<String> fileList = new ArrayList<String>();
        File dir = new File(baseRoot.concat(dirPath));
        String rName;
        if (dir.exists() && dir.isDirectory()) {
            File[] subFile = dir.listFiles();
            for (File sub : subFile) {
                if (sub.exists()) {
                    rName = dirPath.concat("/").concat(sub.getName());
                    if (sub.isFile()) {
                        fileList.add(rName);
                    } else if (sub.isDirectory()) {
                        fileList.addAll(listFiles(rName, baseRoot));
                    }
                }
            }
        }
        return fileList;
    }
    
}
