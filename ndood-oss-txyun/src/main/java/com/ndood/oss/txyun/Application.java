package com.ndood.oss.txyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * https://gitee.com/xuliugen/oss-demo
 * https://github.com/QcloudApi/qcloudapi-sdk-java
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
