package com.ndood.oss.aliyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * https://github.com/xiiiblue/demo-oss
 * https://github.com/fanglijun/aliyun-oss-demo
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
