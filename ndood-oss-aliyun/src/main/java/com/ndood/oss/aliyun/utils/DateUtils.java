package com.ndood.oss.aliyun.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 日期相关实用工具类
 */
public class DateUtils {
    protected static Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
    public static final SimpleDateFormat FORMAT_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final Long MILLIS_PER_DAY = 86400000l;

    /**
     * 解析文本日期为日期类型
     *
     * @param datetime
     * @return
     */
    public static java.util.Date parseDatetime(String datetime) {
        if (StringUtils.isEmpty(datetime))
            return null;

        try {
            return FORMAT_DATETIME.parse(datetime);
        } catch (ParseException e) {
            throw new RuntimeException("日期格式错误，请确认日期格式是否为 yyyy-MM-dd HH:mm:ss");
        }
    }

    /**
     * 日期类型转换为文本格式 yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static String formatDatetime(Date date) {
        if (date == null) {
            return null;
        }
        return FORMAT_DATETIME.format(date);
    }


    /**
     * @return 返回java.util.Date的当前日期
     */
    public static java.util.Date getCurrentDate() {
        return new Date();
    }

    // 秒转换成两位的时间，格式：HH:mm:ss
    public static String turnSecondsToTimestring(int seconds) {
        String result = "";
        int hour = 0, min = 0, second = 0;
        hour = seconds / 3600;
        min = (seconds - hour * 3600) / 60;
        second = seconds - hour * 3600 - min * 60;
        if (hour < 10) {
            result += "0" + hour + ":";
        } else {
            result += hour + ":";
        }
        if (min < 10) {
            result += "0" + min + ":";
        } else {
            result += min + ":";
        }
        if (second < 10) {
            result += "0" + second;
        } else {
            result += second;
        }
        return result;
    }

    // 秒转换成两位的时间，格式：HH:mm:ss
    public static String turnSecondsToTimestring(Long seconds) {
        String result = "";
        Long hour = 0L, min = 0L, second = 0L;
        hour = seconds / 3600;
        min = (seconds - hour * 3600) / 60;
        second = seconds - hour * 3600 - min * 60;
        if (hour < 10) {
            result += "0" + hour + ":";
        } else {
            result += hour + ":";
        }
        if (min < 10) {
            result += "0" + min + ":";
        } else {
            result += min + ":";
        }
        if (second < 10) {
            result += "0" + second;
        } else {
            result += second;
        }
        return result;
    }

    /**
     * 获取当前时间
     * @return
     */
	public static String getCurrentTimestamp() {
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return fm.format(new Date());
	}
}
