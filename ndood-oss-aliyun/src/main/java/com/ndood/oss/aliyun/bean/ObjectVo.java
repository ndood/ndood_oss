package com.ndood.oss.aliyun.bean;

import java.io.Serializable;

public class ObjectVo implements Serializable {
	private static final long serialVersionUID = -8845418390890664914L;
	private String code;
	private String desc;
	private Object obj;

	public ObjectVo(String code, String desc) {
		super();
		this.code = code;
		this.desc = desc;
	}

	public ObjectVo(String code, String desc, Object obj) {
		super();
		this.code = code;
		this.desc = desc;
		this.obj = obj;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

}
